//
//  ViewController.swift
//  AppDev
//
//  Created by Clinton Jay Ramonida on 2017-06-28.
//  Copyright © 2017 Clinton Jay Ramonida. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    
    @IBOutlet weak var button1: UIButton!
    
    @IBOutlet weak var button2: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Develop branch
        
        //Feature branch for comments	
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func button1Action(_ sender: Any) {
    
        label.text = "New Text"
    }

    @IBAction func button2Action(_ sender: Any) {
    }
}
